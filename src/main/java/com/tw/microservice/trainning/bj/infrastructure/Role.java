package com.tw.microservice.trainning.bj.infrastructure;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@ToString
@Table(name = "t_role")
public class Role implements Serializable {

    public enum Symbol {
        STAFF("门店店员"),
        OPS("营运"),
        FINANCE("财务"),
        CUSTOMER_SERVICE("客服"),
        MANAGER("经理"),
        CUSTOMER("客户");

        private String description;

        Symbol(String description) {
            this.description = description;
        }

        public String description() {
            return description;
        }
    }

    @Id
    @Column(unique = true)
    @Enumerated(EnumType.STRING)
    private Symbol symbol;

    private String name;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinTable(name = "t_role_privilege", joinColumns = @JoinColumn(name = "role_symbol", referencedColumnName = "symbol"),
            inverseJoinColumns = @JoinColumn(name = "privilege_symbol", referencedColumnName = "symbol"))
    private List<Privilege> privileges = new ArrayList<>();

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
        name = symbol.description();
    }
}
