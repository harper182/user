package com.tw.microservice.trainning.bj.application;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginCommand {
    private String name;
    private String password;
}
