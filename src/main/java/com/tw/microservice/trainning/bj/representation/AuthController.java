package com.tw.microservice.trainning.bj.representation;

import com.tw.microservice.trainning.bj.application.ApplicationService;
import com.tw.microservice.trainning.bj.application.LoginCommand;
import com.tw.microservice.trainning.bj.configuration.JWTUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping("/api")
public class AuthController {

    @Autowired
    private ApplicationService applicationService;

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public JWTUser login(HttpServletResponse response, @RequestBody LoginCommand loginCommand) throws Exception {
        log.info("User login: {}", loginCommand.getName());

        return applicationService.login(response, loginCommand);
    }

    @DeleteMapping("/logout")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logout(HttpServletRequest request) {
        log.info("User logout");

        applicationService.logout(request);
    }
}
