package com.tw.microservice.trainning.bj.configuration;

public final class APISecureRolePrivilege {

    public static final String CREATE_USER = "ROLE_CREATE_USER";
    public static final String RETRIVE_USER = "ROLE_RETRIVE_USER";

}
