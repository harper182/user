package com.tw.microservice.trainning.bj.domain;

import com.tw.microservice.trainning.bj.application.exception.InvalidCredentialException;
import com.tw.microservice.trainning.bj.configuration.JWTUser;
import com.tw.microservice.trainning.bj.infrastructure.Privilege;
import com.tw.microservice.trainning.bj.infrastructure.User;
import com.tw.microservice.trainning.bj.utils.StringUtils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.tw.microservice.trainning.bj.application.ApplicationService.JWT_PREFIX;

@Service
public class AuthService {
    private static final String PREFIX_BLACK_LIST = "USER-BLACKLIST-";

    public static Map<String, String> blackList = new HashMap<>();

    @Value("${security.jwt.secret:_USER_JWT_SECRET}")
    private String jwtSecret;

    @Value("${security.jwt.expiration-in-seconds: 6000}")
    private long expirationInSeconds;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public void verifyLoginInfo(User user) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getName(), user.getPassword()));
    }

    public String generateJwt(final User user) {
        List<Privilege.Symbol> privileges = user.getRole().getPrivileges().stream()
                .map(Privilege::getSymbol).collect(Collectors.toList());
        Map<String, Object> payload = new HashMap<>();
        payload.put("privileges", privileges);
        payload.put("username", user.getName());
        payload.put("role", user.getRole().getSymbol());

        return generateToken(payload);
    }

    public void logout(HttpServletRequest request) {
        String token = extractToken(request);
        String key = PREFIX_BLACK_LIST + token;
//        redisTemplate.opsForValue().set(key, token);
//        redisTemplate.expire(key, expirationInSeconds, TimeUnit.SECONDS);
        blackList.put(key, token);
    }

    public JWTUser getAuthorizedJWTUser(HttpServletRequest request) {
        String payload = extractAuthorizedPayload(extractToken(request));
        return StringUtils.readJsonStringAsObject(payload, JWTUser.class);
    }

    public boolean hasJWTToken(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        return StringUtils.hasText(authorizationHeader) && authorizationHeader.startsWith(JWT_PREFIX);
    }

    public boolean isTokenInBlackList(HttpServletRequest request) {
        String token = extractToken(request);

//        return StringUtils.hasText(redisTemplate.opsForValue().get(PREFIX_BLACK_LIST + token));
        return blackList.containsKey(PREFIX_BLACK_LIST + token);
    }

    private String extractToken(HttpServletRequest request) {
        if (!hasJWTToken(request)) {
            throw new InvalidCredentialException();
        }
        return request.getHeader(HttpHeaders.AUTHORIZATION).substring(JWT_PREFIX.length() + 1);
    }

    private String generateToken(Map<String, Object> payload) {
        return Jwts.builder()
                .setClaims(payload)
                .setExpiration(new Date(System.currentTimeMillis() + expirationInSeconds * 1000))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    private String extractAuthorizedPayload(String jwtToken) {
        return StringUtils.writeObjectAsJsonString(Jwts.parser().setSigningKey(jwtSecret)
                .parseClaimsJws(jwtToken)
                .getBody());
    }
}
