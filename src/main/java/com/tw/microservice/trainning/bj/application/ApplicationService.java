package com.tw.microservice.trainning.bj.application;

import com.tw.microservice.trainning.bj.configuration.JWTUser;
import com.tw.microservice.trainning.bj.domain.AuthService;
import com.tw.microservice.trainning.bj.infrastructure.Privilege;
import com.tw.microservice.trainning.bj.infrastructure.User;
import com.tw.microservice.trainning.bj.infrastructure.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Collectors;

@Service
public class ApplicationService {

    public static final String JWT_PREFIX = "Bearer";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @Transactional(readOnly = true)
    public JWTUser login(HttpServletResponse response, LoginCommand loginCommand) {
        User user = new User();
        user.setName(loginCommand.getName());
        user.setPassword(loginCommand.getPassword());
        authService.verifyLoginInfo(user);

        User authenticatedUser = userRepository.findByName(user.getName());
        String jwt = authService.generateJwt(authenticatedUser);
        response.addHeader(HttpHeaders.AUTHORIZATION, String.join(" ", JWT_PREFIX, jwt));

        return toJwtUser(authenticatedUser);
    }

    public void logout(HttpServletRequest request) {
        authService.logout(request);
    }

    private JWTUser toJwtUser(User user) {
        JWTUser jwtUser = new JWTUser();
        jwtUser.setUsername(user.getName());
        jwtUser.setRole(user.getRole().getSymbol().name());
        jwtUser.setPrivileges(user.getRole().getPrivileges().stream()
                .map(Privilege::getSymbol).collect(Collectors.toList()));

        return jwtUser;
    }
}
