FROM frolvlad/alpine-oraclejdk8
VOLUME /tmp
ADD build/libs/user-0.0.1-SNAPSHOT.jar app.jar
RUN sh -c 'touch app.jar'
ENV JAVA_OPTS=""
CMD [ "sh", "-c", "java ˙ -Djava.security.egd=file:/dev/./urandom -jar app.jar" ]
